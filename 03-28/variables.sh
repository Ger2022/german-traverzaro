#!/bin/bash

echo "El usuario es: $USER"
echo "El directorio de usuario es: $HOME"
echo "La shell utilizada es: $SHELL"
echo "El host del equipo es: $HOSTNAME"

FECHA=$(date)
echo "Fecha y hora actual: $FECHA"

export MATERIA=Programacion
echo "La materia que se esta cursando es: $MATERIA"

#EOF
